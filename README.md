Create a Conda environment using the command: `mamba env create -f environment.yml`.

Activate the environment, and execute the command `jupyter lab` to run the JupyterLab.

You can use the https://myhdf5.hdfgroup.org website to explore the contents of the HDF5 files.
